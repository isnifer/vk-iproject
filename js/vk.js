angular
  .module('VKApp', [])
  .config(function ($routeProvider){
    // Настроим маршруты
    $routeProvider
      // Main
      .when('/', {
        templateUrl: '../templates/main.html',
        controller: 'Main'
      })
  })
  // Main Controller
  .controller('Main', function($scope, $http){

    $scope.token = localStorage.getItem('accessToken');
    $scope.userId = localStorage.getItem('userId');

    $http
      .jsonp('https://api.vk.com/method/audio.get?count=20&access_token='+ $scope.token +'&callback=JSON_CALLBACK')
      .success(function(data){
        $scope.tracks = data.response;
      });

  });